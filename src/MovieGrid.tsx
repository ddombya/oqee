import React, { useState, useEffect } from "react";
import axios from "axios";

export const MovieGrid: React.FC = () => {
  const [movies, setMovies] = useState<any[]>([]);
  const [selectedIndex, setSelectedIndex] = useState<number>(0);
  const [error, setError] = useState<any>(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const cols = 5;

  useEffect(() => {
    axios
      .get(
        "https://gist.githubusercontent.com/el-gringo/8ad644ba9d46fb898a812a0c43f9858a/raw/96c48f96159cb6d8367bad8bc6e18119ee184888/movies.json"
      )
      .then((res) => {
        setMovies(res.data);
        setIsLoaded(true);
      })
      .catch((error) => {
        setError(error);
        setIsLoaded(true);
      });
  }, []);

  useEffect(() => {
    const handleKeyDown = (event: any) => {
      if (event.key === "ArrowLeft") {
        setSelectedIndex((prevIndex) => {
          return (prevIndex - 1 + movies.length) % movies.length;
        });
      } else if (event.key === "ArrowRight") {
        setSelectedIndex((prevIndex) => {
          return (prevIndex + 1) % movies.length;
        });
      } else if (event.key === "ArrowUp") {
        setSelectedIndex((prevIndex) => {
          return (prevIndex - cols + movies.length) % movies.length;
        });
      } else if (event.key === "ArrowDown") {
        setSelectedIndex((prevIndex) => {
          return (prevIndex + cols) % movies.length;
        });
      }
    };

    document.addEventListener("keydown", handleKeyDown);

    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };
  }, [movies]);

  if (error) {
    return <div>Error: {error?.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else
    return (
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {movies.map((movie, index) => {
          const isSelectedStyles =
            selectedIndex === index
              ? {
                  content: '""',
                  border: `4px solid red`,
                  zIndex: 1,
                }
              : {};
          return (
            <div
              key={movie.id}
              style={{
                width: "19%",
                height: "auto",
                padding: 8,
              }}
            >
              <img
                src={movie.poster}
                alt={movie.title}
                style={isSelectedStyles}
                id={selectedIndex.toString()}
              />
            </div>
          );
        })}
      </div>
    );
};
