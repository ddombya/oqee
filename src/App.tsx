import React from "react";
import "./App.css";
import { MovieGrid } from "./MovieGrid";

export const App: React.FC = () => {
  return <MovieGrid />;
};
